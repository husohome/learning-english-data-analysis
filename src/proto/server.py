import time
from concurrent import futures

import config
import grpc
import test_pb2
import test_pb2_grpc
import user_pb2
import user_pb2_grpc
from compile_grpc import compile_protos


class UserService(user_pb2_grpc.UserService):
    def __init__(self, *args, **kwargs):
        ...

    def GetUserMicro(self, request, target):
        id = request.id
        print(f"[Python] recv id from client: {request.id}")
        response = {
            "statusCode": 200,
            "message": f"get user micro: {id}",
            "error": "test",
            "data": "test",
        }
        return user_pb2.ResponseUser(**response)


class TestService(test_pb2_grpc.TestService):
    def __init__(self, *args, **kwargs):
        ...

    def GetServerResponseMicro(self, request, context):
        print(f"[Python] message from client: {request.message}")
        result = {"message": f"[{request.message}]", "received": True}
        return test_pb2.MessageResponse(**result)


def serve():
    print("on serve...")

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    test_pb2_grpc.add_TestServiceServicer_to_server(TestService(), server)
    user_pb2_grpc.add_UserServiceServicer_to_server(UserService(), server)

    server.add_insecure_port(f"[::]:{config.MICRO_TEST_PORT}")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    print(f"server.py... {config.MICRO_TEST_HOST}:{config.MICRO_TEST_PORT}")
    serve()
