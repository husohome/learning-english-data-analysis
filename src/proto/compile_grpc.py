import glob
import os
import subprocess
from typing import List


def strip_all(content: str) -> List[str]:
    output = ""
    for line in content.split("\n"):
        line = line.strip()
        output += line + "\n"
    return output


def create_main_proto():
    protos = [os.path.basename(p) for p in glob.glob("*.proto") if "main.proto" not in p]
    import_protos_template = "\n".join([f'import "{proto}";' for proto in protos])

    content = f"""
    syntax = "proto3";

    {import_protos_template}

    package PACKAGE;
    """.strip()

    with open("main.proto", "w") as f:
        f.write(strip_all(content))


def compile_protos():
    protos = [os.path.basename(p) for p in glob.glob("*.proto") if "main.proto" not in p]
    lines = []

    for proto in protos:
        print(f"compile: {proto}")
        lines.append(
            f"python -m grpc_tools.protoc --proto_path=. ./{proto} --python_out=. --grpc_python_out=."
        )

    for line in lines:
        subprocess.check_call(line, shell=True)


if __name__ == "__main__":
    create_main_proto()
    compile_protos()
