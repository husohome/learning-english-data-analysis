import random

import base_pb2
import base_pb2_grpc
import config
import grpc
import test_pb2
import test_pb2_grpc
import user_pb2
import user_pb2_grpc
from compile_grpc import compile_protos


class TestClient(object):
    def __init__(self):
        self.host = config.MICRO_TEST_HOST
        self.port = config.MICRO_TEST_PORT

        print(f"self.host: {self.host}, self.port: {self.port}")

        # instantiate a channel
        self.channel = grpc.insecure_channel(f"{self.host}:{self.port}")

        # bind the client and the server
        self.stub = test_pb2_grpc.TestServiceStub(self.channel)
        self.user = user_pb2_grpc.UserServiceStub(self.channel)

    def get_server_response(self, message):
        response = test_pb2.MessageRequest(message=message)
        return self.stub.GetServerResponseMicro(response)

    def get_user(self, id="1000"):
        response = base_pb2.ById(id=id)
        return self.user.GetUserMicro(response)


if __name__ == "__main__":
    compile_protos()
    test_client = TestClient()
    result = test_client.get_server_response(message="test message...")
    print(f"result: {result}")
    user = test_client.get_user(id=random.randint(1, 2000).__str__())
    print(f"user: {user}")
